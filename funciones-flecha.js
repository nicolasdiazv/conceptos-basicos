const add = (a,b) => a + b 
const subtract = (a,b) => a - b 
const multiply = (a,b) => a * b 
const divide = (a,b) => {
  if(b === 0) throw new Error('Cannot be divided by 0')
  return a / b
} 

const x = 4
const y = 3
const zero = 0

console.log(`${x} + ${y} = `,add(x,y))
console.log(`${x} - ${y} = `,subtract(x,y))
console.log(`${x} * ${y} = `,multiply(x,y))
console.log(`${x} / ${y} = `,divide(x,y))
