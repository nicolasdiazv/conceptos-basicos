# Conceptos básicos
- Let vs Var
    - El ciclo de vida de las variables (ámbito o scope).
- Funciones de Flecha
    - Operaciones matemáticas
        - Suma
        - Resta
        - Multiplicación
        - División